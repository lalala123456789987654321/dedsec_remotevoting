<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Instructions for voting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <script src="main.js"></script>
</head>
<body>
<ul>
  <li><a class="active" href="index.php">Home</a></li>
  <li><a href="Instructions.php">Instructions</a></li>
  <li><a href="ElectionCommissionBoard.php">Election Commission Board</a></li>
  <li><a href="FAQ.php">FAQ</a></li>
  <li><a href="ContactHelp.php">Contact/Help</a></li>
  <li><a href="Results.php">Results</a></li>
</ul>

</body>
</html>