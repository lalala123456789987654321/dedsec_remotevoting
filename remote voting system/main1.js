const SHA256=require('crypto-js/sha256');


var today=new Date();
var timestamp=today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time=today.getHours()+':'+today.getMinutes()+':'+today.getSeconds()+':'+today.getMilliseconds();


class Vote
{

    constructor(index, vote, previousHash='', password, fullname, aadhar)
    {
        this.index=index;
        this.vote=vote;
        this.previousHash=previousHash;
        this.password=password;
        this.fullname=fullname;
        this.aadhar=aadhar;
        this.hash=this.calculateHash();
    }


    calculateHash()
    {
        return SHA256(this.index + this.previousHash + this.vote + this.timestamp + this.time + this.password + this.fullname + this.aadhar).toString();
    }

    
}

class Blockchain
{
    constructor()
    {
        this.chain=[this.createGenesisVote()];
    }
    createGenesisVote()
    {
        return new Vote(0, JSON.stringify(null), 0, JSON.stringify(null), JSON.stringify(null), JSON.stringify("000000000000"));
    }
    getLatestVote()
    {
        return this.chain[this.chain.length-1];
    }
    addVote(newVote)
    {
        newVote.previousHash=this.getLatestVote().hash;
        newVote.hash= newVote.calculateHash();
        this.chain.push(newVote);
    }
}

let Votes= new Blockchain();
Votes.addVote(new Vote(1, "BJP", "1", "abcde","0123456789", "123456789012"));
Votes.addVote(new Vote(2, "congress", "2", "cdefg","2113456789", "678901234532"));
Votes.addVote(new Vote(3, "aap", "3", "decfh","45456767123", "345678901212"));
Votes.addVote(new Vote(4, "BJP", "4", "tytyir","01234564577", "123456722212"));
Votes.addVote(new Vote(5, "BJP", "5", "wuwuow","0123434531", "123412341234"));
//take from user, use for loop


console.log(JSON.stringify(Votes, null, 4));



