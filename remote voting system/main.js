const SHA256=require('crypto-js/sha256');


var today=new Date();
var timestamp=today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time=today.getHours()+':'+today.getMinutes()+':'+today.getSeconds()+':'+today.getMilliseconds();


class Vote
{

    constructor(index, vote, previousHash='', password, fullname, aadhar)
    {
        this.index=index;
        this.vote=vote;
        this.previousHash=previousHash;
        this.password=password;
        this.fullname=fullname;
        this.aadhar=aadhar;
        this.hash=this.calculateHash();
    }


    calculateHash()
    {
        return SHA256(this.index + this.previousHash + this.vote + this.timestamp + this.time + this.password + this.fullname + this.aadhar).toString();
    }

    
}

class Blockchain
{
    constructor()
    {
        this.chain=[this.createGenesisVote()];
    }
    createGenesisVote()
    {
        return new Vote(0, JSON.stringify(null), 0, JSON.stringify(null), JSON.stringify(null), JSON.stringify("000000000000"));
    }
    getLatestVote()
    {
        return this.chain[this.chain.length-1];
    }
    addVote(newVote)
    {
        newVote.previousHash=this.getLatestVote().hash;
        newVote.hash= newVote.calculateHash();
        this.chain.push(newVote);
    }
}

let Votes= new Blockchain();
Votes.addVote(new Vote(1, "BJP", "1", "abcde", "bittu", "123456789123"));
//take from user, use for loop


console.log(JSON.stringify(Votes, null, 2));



